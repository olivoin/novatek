$(document).ready(function() {
	$('#header .burger').click(function() {
		$('#header .mobile-nav,#header .overlay').toggleClass('open');
		$(this).toggleClass('active');
	});

	$('#header .overlay').click(function() {
		$('#header .mobile-nav,#header .overlay').removeClass('open');
		$('#header .burger').removeClass('active');
	});
});